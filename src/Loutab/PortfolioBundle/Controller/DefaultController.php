<?php

namespace Loutab\PortfolioBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DefaultController extends Controller
{
    public function indexAction()
    {
        return $this->render('LoutabPortfolioBundle:Default:index.html.twig');
    }
}
